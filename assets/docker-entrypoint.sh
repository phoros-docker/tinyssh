#!/bin/sh

# Environment checks
if [ -z "$PUBLIC_KEY" ]; then
    echo >&2 "No public key defined."
    exit 1
fi

if [ ! -f /initialized ]; then
    # Generate host key
    ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
    # Create SSH directory, put the public key in there and set the correct permissions
    mkdir -p /home/tinyssh/.ssh/
    chown tinyssh:tinyssh /home/tinyssh/.ssh/
    chmod 700 /home/tinyssh/.ssh/
    echo "$PUBLIC_KEY" >> /home/tinyssh/.ssh/authorized_keys
    chown tinyssh:tinyssh /home/tinyssh/.ssh/authorized_keys
    chmod 644 /home/tinyssh/.ssh/authorized_keys

    touch /initialized
fi

exec "$@"
