FROM alpine:edge

# Install main packages and build-dependencies and do some other stuff
RUN addgroup tinyssh \
    && adduser -D -s /bin/sh -h "/home/tinyssh" -G tinyssh tinyssh \
    && echo "tinyssh:$(< /dev/urandom tr -cd '[:alnum:]' | head -c 64)" | chpasswd \
    && apk add --update --no-cache --no-progress openssh-server openssh-sftp-server \
    && sed -e 's|#\(HostKey /etc/ssh/ssh_host_ed25519_key\)|\1|' \
           -e 's/#PermitRootLogin prohibit-password/PermitRootLogin no/' \
           -e 's/#\(StrictModes yes\)/\1/' \
           -e 's/#\(PubkeyAuthentication yes\)/\1/' \
           -e 's/#\(PasswordAuthentication yes\)/\1/' \
           -i /etc/ssh/sshd_config

# Copy init script
COPY ./assets/docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD ["/usr/sbin/sshd","-D","-e","-f","/etc/ssh/sshd_config"]
EXPOSE 22
